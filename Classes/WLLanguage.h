//
//  WLLanguage.h
//  YuGarden
//
//  Created by 王陆 on 2018/12/11.
//  Copyright © 2018 南京圈圈网络科技. All rights reserved.
//
#import <Foundation/Foundation.h>
typedef NS_ENUM(NSUInteger, WLLanguageUseType) {
    WLLanguageUseOSPriority,
    WLLanguageUseApplicationPriority
};
@interface WLLanguage : NSObject
/*
 获取当前资源文件
 */
+ (NSBundle *)bundle;
/*
 初始化语言文件
 */
+ (void)initializeLanguage;
/*
获取当前应用语言
 */
+ (NSString *)applicationLanguage;
/*
 设置当前应用语言
 */
+ (void)setupApplicationLanguage:(NSString *)languageCode;
@end
