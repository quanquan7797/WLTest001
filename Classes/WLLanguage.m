//
//  WLLanguage.m
//  YuGarden
//
//  Created by 王陆 on 2018/12/11.
//  Copyright © 2018 南京圈圈网络科技. All rights reserved.
//

#define kLocalLanguageKey  @"LocalLanguageKey"
#define kLanguageType(code) [[[NSLocale preferredLanguages] firstObject] hasPrefix:code]
#import "WLLanguage.h"
#import "WLLanguageCode.h"

@implementation WLLanguage
static NSBundle * bundle = nil;
+ (NSBundle *)bundle {
    [WLLanguage initializeLanguage];
    return bundle;
}
/*
首次加载时需要检测语言是否存在
 */
+ (void)initializeLanguage {
    NSUserDefaults * def = [NSUserDefaults standardUserDefaults];
    NSString * currLanguage = [def valueForKey:kLocalLanguageKey];
    if (!currLanguage) { // 用户未选择语言
        if (kLanguageType(kChineseCode)) {
            currLanguage = kChineseCode;
        } else if (kLanguageType(kEnglishCode)) {
            currLanguage = kEnglishCode;
        }
    }
    if (currLanguage) {
        [def setValue:currLanguage forKey:kLocalLanguageKey];
        [def synchronize];
    }
    NSString * path = [[NSBundle mainBundle] pathForResource:currLanguage ofType:@"lproj"];
    bundle = [NSBundle bundleWithPath:path];
}
/*
 获取当前应用语言
 */
+ (NSString *)applicationLanguage {
    NSUserDefaults * def = [NSUserDefaults standardUserDefaults];
    NSString * language = [def objectForKey:kLocalLanguageKey];
    return language;
}
/*
设置应用语言
 */
+ (void)setupApplicationLanguage:(NSString *)languageCode {
    NSUserDefaults * def = [NSUserDefaults standardUserDefaults];
    NSString * currentLanguage = [def valueForKey:kLocalLanguageKey];
    if ([currentLanguage isEqualToString:languageCode]) {
        return;
    }
    [def setValue:languageCode forKey:kLocalLanguageKey];
    [def synchronize];

    NSString * path = [[NSBundle mainBundle] pathForResource:languageCode ofType:@"lproj"];
    bundle = [NSBundle bundleWithPath:path];
}
@end
