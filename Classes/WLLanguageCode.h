//
//  WLLanguageCode.h
//  YuGarden
//
//  Created by 王陆 on 2018/12/11.
//  Copyright © 2018 南京圈圈网络科技. All rights reserved.
//

#import <Foundation/Foundation.h>
extern NSString * const kChineseCode;             // 简体中文
extern  NSString * const kHantChineseCode;    //  繁体中文
extern NSString * const kEnglishCode;             // 英文
extern NSString * const kJapaneseCode;           // 日文
extern NSString * const kSpanishCode;             // 西班牙语
extern NSString * const kMexicoSpanishCode; // 墨西哥西班牙语
extern NSString * const kItalianCode;    // 意大利语
extern NSString * const kDutchCode;     // 荷兰语
extern NSString * const kKoreanCode;   // 韩语
extern NSString * const kBrazilPortugueseCode; // 巴西葡萄牙语
extern NSString * const kPortugalPortugueseCode; // 葡萄牙语
extern NSString * const kDanishCode;   // 丹麦语
extern NSString * const kFinnishCode;     // 丹麦语



