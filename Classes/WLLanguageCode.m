//
//  WLLanguageCode.m
//  YuGarden
//
//  Created by 王陆 on 2018/12/11.
//  Copyright © 2018 南京圈圈网络科技. All rights reserved.
//

#import "WLLanguageCode.h"
 NSString * const kChineseCode = @"zh-Hans";             // 简体中文
 NSString * const kHantChineseCode = @"zh-Hant";    //  繁体中文
 NSString * const kEnglishCode = @"en";             // 英文
 NSString * const kJapaneseCode = @"ja";           // 日文
 NSString * const kSpanishCode = @"es";             // 西班牙语
 NSString * const kMexicoSpanishCode = @"es-MX"; // 墨西哥西班牙语
 NSString * const kItalianCode = @"it";    // 意大利语
 NSString * const kDutchCode = @"nl";     // 荷兰语
 NSString * const kKoreanCode = @"ko";   // 韩语
 NSString * const kBrazilPortugueseCode = @"pt-BR"; // 巴西葡萄牙语
 NSString * const kPortugalPortugueseCode = @"pt-PT"; // 葡萄牙语
 NSString * const kDanishCode = @"da";   // 丹麦语
 NSString * const kFinnishCode = @"fi";     // 丹麦语

