//
//  WLLocalized.h
//  YuGarden
//
//  Created by 王陆 on 2018/12/11.
//  Copyright © 2018 南京圈圈网络科技. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface WLLocalized : NSObject
+ (NSString *)localizedForKey:(NSString *)key table:(NSString *)tbl;
+ (void)setupApplicationLanguage:(NSString *)languageCode;
@end

