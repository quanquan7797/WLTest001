//
//  WLLocalized.m
//  YuGarden
//
//  Created by 王陆 on 2018/12/11.
//  Copyright © 2018 南京圈圈网络科技. All rights reserved.
//
#define kLocalized(key,tbl) [[WLLanguage bundle] localizedStringForKey:key value:@"" table:tbl]
#import "WLLocalized.h"
#import "WLLanguage.h"

@implementation WLLocalized
+ (NSString *)localizedForKey:(NSString *)key table:(NSString *)tbl{
    return kLocalized(key, tbl);
}
+ (void)setupApplicationLanguage:(NSString *)languageCode {
    [WLLanguage setupApplicationLanguage:languageCode];
}
@end
