//
//  WLYuGardenLocalized.h
//  YuGarden
//
//  Created by 王陆 on 2018/12/13.
//  Copyright © 2018 南京圈圈网络科技. All rights reserved.
//

#import "WLLocalized.h"
@interface WLYuGardenLocalized : WLLocalized
+ (NSString *)localizedForKey:(NSString *)key;
@end

