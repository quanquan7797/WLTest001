//
//  WLYuGardenLocalized.m
//  YuGarden
//
//  Created by 王陆 on 2018/12/13.
//  Copyright © 2018 南京圈圈网络科技. All rights reserved.
//

#define kTbl   @"LocalizeLanguages"
#import "WLYuGardenLocalized.h"

@implementation WLYuGardenLocalized
+ (NSString *)localizedForKey:(NSString *)key {
   return [WLYuGardenLocalized localizedForKey:key table:kTbl];
}
+ (void) setupApplicationLanguage:(NSString *)languageCode {
    [super setupApplicationLanguage:languageCode];
}
@end
